import Home from './components/homePage/Home'
import PrdDetail from './components/products/Product_detail'
import Login from './components/Login'
export default [
    {
        path: '/productinfo/:name/:id',
        component: PrdDetail,
    },
    {
        path: '/home',
        component: Home,
    },
    {
        path: '/',
        component: Home,
    },
    {
        path: '/login',
        component: Login,
    }
]