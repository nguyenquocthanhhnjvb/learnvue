import axios from "axios";

const instance = axios.create({
    baseURL: (process.env.VUE_APP_ROOT_URL_API !== undefined) ? process.env.VUE_APP_ROOT_URL_API : 'http://127.0.0.1:8000'
});
instance.interceptors.request.use(
    (config) => {

        let user = JSON.parse(localStorage.getItem('user'))

        let token = null;
        if (user) {
            token = user.AUTH_TOKEN;
            config.headers['Authorization'] = `Bearer ${token}`;
        }
        return config
    },

    (error) => {
        return Promise.reject(error)
    }
)
export default instance;

// const baseURL = 'http://127.0.0.1:8000';

// export default function callApi(url, method = 'GET', data, token) {
//     console.log(token);
//     let user = JSON.parse(localStorage.getItem('user'))

//     let newToken = null;
//     if (user) {
//         newToken = user.AUTH_TOKEN;
//     }
//     return axios({
//         method: method,
//         url: `${baseURL}/${url}`,
//         data: data,
//         headers: {
//             'Authorization': 'Bearer ' + newToken,
//             'Content-Type': 'application/json',
//             // 'Access-Control-Allow-Origin': '*'
//         }
//     }).catch((err) => {
//         console.log(err);
//     })
// }