
const mutations = {
    isLoggin(state) {
        state.user.isLoggin = true;
    },
    AUTH_TOKEN(state, token) {
        state.user.AUTH_TOKEN = token;
    },
    user(state, userInfo) {
        state.user.name = userInfo.name;
        state.user.email = userInfo.email;
    },
    logout(state) {
        state.user.isLoggin = false;
        state.user.AUTH_TOKEN = '';
        state.user.name = '';
        state.user.email = '';
        localStorage.removeItem('user');
    }
}

export default mutations