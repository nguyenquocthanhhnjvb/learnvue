import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import mutations from './mutations'
Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        user
    },
    mutations
})