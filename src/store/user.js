const getUser = JSON.parse(localStorage.getItem('user'));

var user = {
    name: '',
    email: '',
    isLoggin: '',
    AUTH_TOKEN: '',
}
if (getUser) {
    user = {
        name: getUser.name,
        email: getUser.email,
        isLoggin: getUser.isLoggin,
        AUTH_TOKEN: getUser.AUTH_TOKEN,
    }
}

export default user